
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name:'home', component: () => import('pages/Index.vue') },
      { path: 'Auth', name: 'Auth', component: () => import('pages/Auth.vue'),
        children : [
          { path: 'login', name:'login'},
          { path: 'register', name:'register'},
          { path: 'registerTeacher', name:'registerTeacher'},
          { path: 'forget', name:'forget'},
        ]
      },
      { path: 'test', name:'test', component: () => import('pages/Test.vue') ,
        children : [
          { path: 'writeCbt', name:'writeCbt'},
          { path: 'setCbt', name:'setCbt'},
          { path: 'resultCbt', name:'resultCbt'},
        ]
      },
      { path: 'course', name:'course', component: () => import('pages/Course.vue') ,
        children : [
          { path: 'classroom', name:'classroom'},
          { path: 'list', name:'list'},
        ]
      },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
